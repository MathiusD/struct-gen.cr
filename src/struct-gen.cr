require "bson"
require "json"
require "yaml"
require "git-version-gen/src/git-version-gen.cr"

module StructGen
  GitVersionGen.generate_version __DIR__

  macro attach_serialization_includes
    include JSON::Serializable
    include JSON::Serializable::Strict
    include BSON::Serializable
    include YAML::Serializable
  end

  macro attach_field_annotation(key_value)
    @[JSON::Field(key: {{key_value}})]
    @[BSON::Field(key: {{key_value}})]
    @[YAML::Field(key: {{key_value}})]
  end

  macro generate_with_field(base_module, field_class, field_name, field_type, field_key, is_required)
    module {{base_module}}::With::{% if is_required == false %}Optional::{% end %}{{field_class}}
      StructGen.attach_field_annotation({{field_key}})
      getter {{field_name}} : {{field_type}}{% if is_required == false %}?{% end %}
    end
  end

  macro generate_with_required_field(base_module, field_class, field_name, field_type, field_key)
    StructGen.generate_with_field {{base_module}},
      {{field_class}},
      {{field_name}},
      {{field_type}},
      {{field_key}},
      true
  end

  macro generate_with_optional_field(base_module, field_class, field_name, field_type, field_key)
    StructGen.generate_with_field {{base_module}},
      {{field_class}},
      {{field_name}},
      {{field_type}},
      {{field_key}},
      false
  end

  macro generate_struct(struct_name, *fields)
    struct {{struct_name}}
      StructGen.attach_serialization_includes
      {% for field in fields %}
        {% if field["is_module"] == true %}
          include {{field["base_module"]}}::With::{% if field["is_required"] == false %}Optional::{% end %}{{field["attribute_name"].id.camelcase(lower: false)}}{% if field["module_generics_args"] != nil %}({{field["module_generics_args"]}}){% end %}
        {% else %}
          {% if field["serialized_name"] == nil %}
            StructGen.attach_field_annotation "{{field["attribute_name"].id.camelcase(lower: true)}}"
          {% else %}
            StructGen.attach_field_annotation {{field["serialized_name"]}}
          {% end %}
          getter {{field["attribute_name"]}} : {{field["attribute_class"]}}{% if field["is_required"] == false %}?{% end %}
        {% end %}
      {% end %}
      def initialize(
      {% for field in fields %}
        @{{field["attribute_name"]}} : {{field["attribute_class"]}}{% if field["is_required"] == false %}?{% end %},
      {% end %}
      )
      end

      def [](value : String)
        case value
          {% for field in fields %}
            when "{{field["attribute_name"]}}"
              return {{field["attribute_name"]}}
          {% end %}
        else
          return nil
        end
      end
    end
  end
end
