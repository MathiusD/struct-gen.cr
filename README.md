# struct-gen.cr

[![Pipeline Status](https://gitlab.com/MathiusD/struct-gen.cr/badges/master/pipeline.svg)](https://gitlab.com/MathiusD/struct-gen.cr/-/pipelines)
[![Documentation](https://img.shields.io/badge/docs-available-brightgreen.svg)](https://mathiusd.gitlab.io/struct-gen.cr)

struct-gen allow to generate struct with some methods and attributes
