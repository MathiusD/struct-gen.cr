require "./spec_helper"

# Sample generated structs

StructGen.generate_with_optional_field JohnDoe, Bar, bar, UInt16, "bar"

StructGen.generate_with_required_field JohnDoe, FooBar(T), foo_bar, T, "fooBar"

StructGen.generate_struct JohnDoe::Example, {
  is_module:       false,
  attribute_name:  foo,
  attribute_class: String,
}, {
  is_module:       true,
  is_required:     false,
  base_module:     JohnDoe,
  attribute_name:  bar,
  attribute_class: UInt16,
}, {
  is_module:            true,
  base_module:          JohnDoe,
  module_generics_args: Hash(String, Int32),
  attribute_name:       foo_bar,
  attribute_class:      Hash(String, Int32),
}, {
  is_module:       false,
  is_required:     false,
  serialized_name: "example",
  attribute_name:  eXAmple,
  attribute_class: String,
}

describe StructGen do
  # Init
  custom_hash = Hash(String, Int32).new
  custom_hash["some_example"] = 24
  example = JohnDoe::Example.new "foo", 12, custom_hash, "example"

  it "JohnDoe::Example#getter" do
    example.foo.should eq("foo")
    example.bar.should eq(12)
    example.foo_bar.should eq(custom_hash)
    example.eXAmple.should eq("example")
  end

  it "JohnDoe::Example#[]" do
    example["foo"].should eq("foo")
    example["bar"].should eq(12)
    example["foo_bar"].should eq(custom_hash)
    example["eXAmple"].should eq("example")
    example["some_example"].should be_nil
  end

  it "JohnDoe::Example#to_json" do
    example.to_json.should eq("{\"fooBar\":{\"some_example\":24},\"bar\":12,\"foo\":\"foo\",\"example\":\"example\"}")
  end

  it "JohnDoe::Example#to_yaml" do
    example.to_yaml.should eq("---\nfooBar:\n  some_example: 24\nbar: 12\nfoo: foo\nexample: example\n")
  end

  it "JohnDoe::Example#to_bson" do
    bson_hash = Hash(String, Hash(String, Int32) | String | UInt16).new
    bson_hash["fooBar"] = custom_hash
    bson_hash["bar"] = UInt16.new 12
    bson_hash["foo"] = "foo"
    bson_hash["example"] = "example"

    example.to_bson.should eq(BSON.new(bson_hash))
  end
end
